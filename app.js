var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');

var ws = fs.createWriteStream('out.txt');
var url = "http://bigbasket.com/product/all-categories/";
var url_prefix = "http://bigbasket.com";
var url_page = "/product/facet/get-page/"

request({
		"uri" : url,
	}, function(err, resp, body){
		var $ = cheerio.load(body);
		$('.uiv2-search-category li a').each(function(i, e){
			var pUrl = url_prefix + $(e).attr('href');
			productDetails(pUrl);
			// return false;
		})
	}
);

function productDetails(pUrl){
	request({
		"uri" : pUrl,
	}, function(err, resp, body){
		var sidRegx = /init_facets\(\"(.*?)\", /;
		var match = sidRegx.exec(body);
		if(match && match.length == 2){
			var sid = match[1];
			if(sid){
				extractItemDetails(body, sid);
			} else{
				console.log("could not get sid when running url - " + pUrl);
			}
		}
	});
}

function productDetailsFromPaginationJSON(pUrl){
	request({
		"uri" : pUrl,
	}, function(err, resp, body){
		var respJSON = JSON.parse(body);
		var newBody = respJSON.pagination;
		newBody += respJSON.products;
		var sid = respJSON.sid;
		if(sid){
			extractItemDetails(newBody, sid);
		}
		else{
			console.log("could not get sid when running url - " + pUrl);
		}
	});
}

function extractItemDetails(prodHtml, sid){
	var $ = cheerio.load(prodHtml);
	$('.uiv2-our-recommendations-list-box li:not(.uiv2-separator)').each(
		function(i, e){
			var sku = $(e);
			var skuName = sku.find('.uiv2-tool-tip-hover').text().trim();
			var brand = sku.find('.uiv2-brand-title').text().trim();
			var sizeBox = sku.find('.uiv2-field-wrap');
			var size = '';
			// ws.write(sizeBox.length + ' \n');
			// ws.write(sizeBox.find('select option:selected').length + ' \n');
			if(sizeBox.length && sizeBox.find('select option:selected').length){
				// ws.write('Adding line for - ' + skuName + ' \n');
				size = sizeBox.find('select option:selected').text().trim();
			}else{
				size = sizeBox.text().trim();
			}
			var re = /^Rs[.] /i;
			var cost = sku.find('.uiv2-rate-count-avial').text().replace(re, '');
			printItem(skuName, size, cost, brand);
			// return false;
		}
	)

	if($('.pagination .pages li').length > 0){
		var cpage = $('.pagination .pages li a.selected').first();
		var npage = cpage.parent('li').next();
		if(npage.length > 0 && npage.find('a').html() != "Next"){
			var url = url_prefix + url_page + '?sid=' +sid+'&page=' + npage.find('a').html();
			// console.log("getting extra items from - " + url)
;			productDetailsFromPaginationJSON(url);
		}
	}
}

function printItem(name, size, cost, brand){
	ws.write(name+','+ size +','+ cost +','+ brand+',\n');
	// console.log(name+','+ size +','+ cost +','+ brand);
}